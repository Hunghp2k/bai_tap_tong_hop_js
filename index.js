// bài 1
function bai1() {

    var max = 100;

    var content = ``;

    for (var i = 1; i <= max; i++) {
        content += i + " ";
        if (i % 10 === 0) {
            content += `<br>`;
        }
        document.getElementById('txtIn').innerHTML = `${content}`;
    }
}
// bai2
let arr = [];
document.getElementById("themBai2").onclick = function () {
    var them = document.getElementById("txt-bai2").value * 1;

    if (them != "") {
        arr.push(them);
    }
    document.getElementById('txtBai2').innerHTML = arr.join();
}
document.getElementById('thembai2').onclick = function () {
    var arrSoNguyento = arr.filter(num => {
        if (num < 2) {
            return false;
        }
        else {
            for (var i = 2; i < num; i++) {
                if (num % i === 0) {
                    return false
                }
            }
            return true;
        }
    })
    document.getElementById('txtbai2').innerHTML = arrSoNguyento.join();
}
// bài 3
function bai3() {
    var themso = document.getElementById("txt-number").value * 1;
    var tong = 0;
    if (themso === "") {
        return;
    } else {
        for (var i = 2; i <= themso; i++) {
            tong += i;
        }
    }
    document.getElementById("txtbai3").innerHTML = tong + 2 * themso;
}
// bai4

function bai4() {
    var bai4 = document.getElementById('txt-bai4').value * 1;
    var content = "";
    for (var i = 1; i <= bai4; i++) {
        if (bai4 % i === 0) {
            content += i + " ";
        }
    }

    document.getElementById('txtbai4').innerHTML = `Ước của ${bai4}: ${content}`
}

// bai5
function bai5() {
    var bai5 = document.getElementById("txt-bai5").value * 1;
    if (bai5 === 0) {
        return;
    } else {
        document.getElementById("txtbai5").innerHTML = bai5.toString().split("").reverse().join("");
    }
}
// bai6
function bai6() {
    var bai6 = document.getElementById('txt-bai6').value * 1;
    var arr = [];
    var count = 0;

    for (var i = 0; i <= bai6; i++) {
        if (count + 1 < bai6) {
            count += i;
            arr.push(i);
        }
    }
    console.log(arr[-1]);
    document.getElementById('txtbai6').innerHTML = `X = ${arr[arr.length - 1]}`;
}


// bài7
function bai7() {
    var bai7 = document.getElementById('txt-bai7').value * 1;

    var content = ``;

    for (var i = 1; i <= 10; i++) {
        content += `${bai7} * ${i} = ${bai7 * i} </br>`;
    }
    document.getElementById('txtbai7').innerHTML = content;
}
// bài 8
let error = [];
function bai8() {
    var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];
    var content = 0;
    var players = [[], [], [], []];

    cards.forEach(error => {
        if (content > 3) {
            content = 0;
        }
        players[content].push(error);
        content++;
    })

    document.getElementById('player1').innerHTML = `Người chơi 1: ${players[0]}`;
    document.getElementById('player2').innerHTML = `Người chơi 2: ${players[1]}`;
    document.getElementById('player3').innerHTML = `Người chơi 3: ${players[2]}`;
    document.getElementById('player4').innerHTML = `Người chơi 4: ${players[3]}`;
}
// bài 9
function bai9() {
    var tongSoChan = document.getElementById('txt-sochan').value * 1;
    var tongSoGaVaCho = document.getElementById('txt-socon').value * 1;

    if (tongSoChan > 0 && tongSoGaVaCho > 0 && tongSoChan > tongSoGaVaCho) {
        var x = (tongSoChan - (tongSoGaVaCho * 2)) / (2)
        var y = tongSoGaVaCho - x
        document.getElementById('txtbai9').innerHTML = `Tổng số con chó: ${x} , Tổng số con gà: ${y}`
    }
}
// bài 10
function bai10() {
    var sogio = document.getElementById('txt-sogio').value * 1;
    var sophut = document.getElementById('txt-sophut').value * 1;

    var toado = 360;
    var gio = 12;
    var phut = 60;

    if (sogio === '' || sophut === '' || sophut > 60 || sophut < 0 || sogio > 12 || sogio < 0) {
        alert('Xin mời nhập lại')
    }
    else {
        var gocGio = toado / gio * sogio;
        var goPhut = toado / phut * sophut;

        document.getElementById('txtbai10').innerHTML = `Góc lệch giữa kim giờ và kim phút: ${gocGio - goPhut} độ`
    }

}





